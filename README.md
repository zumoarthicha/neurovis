# **NeuroVis: Neurodynamic Visualization** 


NeuroVis is a neural dynamic visualizer that connects to robot operating system (ROS). It acts as a ROS node named “NeuroVis” that receives six topics as inputs: neuron name, connection, updated weight, neuron activity, normalization gain and bias (some of them are optional); therefore, the visualizer is compatible with ROS-supported programming languages, including C++, python and Lua. In addition, the program can be employed along with a simulation, such as CoppeliaSim (VREP), and operate in parallel to the real robot.

<div align="center">
		<a href="https://drive.google.com/file/d/1DmFb3jBahqgajKphoaJtOekd9wuAtqJx/view?usp=sharing">
         <img alt="Qries" src="images/neurovisxmorf.png">
</a>
<p> Example of the visualization of MORF. (Click the picture and see the video.) </p>
</div>

<div align="center">
	<a href="https://drive.google.com/file/d/1MFX_cqiEoUINQAUzkn4UuU6WoeizYSgZ/view?usp=sharing">
         <img alt="Qries" src="images/demo_neurovis.gif">
</a>
<p> Example of the visualization of a 150-neuron neural network (with 400 connections). (Click the picture and see the video.) </p>
</div>

## Directory Tree

```
├── images
│   ├── buttons.jpg
│   ├── demo_neurovis.gif
│   ├── neurovis_example1.gif
│   ├── neurovis_example2.gif
│   ├── neurovisxmorf.png
│   └── rqt_result.jpg
├── neurovis
│   ├── build
│   ├── devel
│   └── src
│       ├── CMakeLists.txt
│       ├── neurovis
│       │   ├── CMakeLists.txt
│       │   ├── configurations
│       │   ├── CrimsonPro.ttf
│       │   ├── images
│       │   ├── launch
│       │   ├── main.py
│       │   ├── NeuroVisNode.py
│       │   ├── NeuroVis.py
│       │   ├── NeuroVis.pyc
│       │   ├── package.xml
│       │   └── __pycache__
│       ├── neurovis_example1
│       │   ├── bin
│       │   ├── CMakeLists.txt
│       │   ├── launch
│       │   ├── package.xml
│       │   ├── README.md
│       │   └── src
│       ├── neurovis_example2
│       │   ├── bin
│       │   ├── CMakeLists.txt
│       │   ├── launch
│       │   ├── package.xml
│       │   └── src
│       └── README.md
├── README.md
└── requirements.txt
```

## Setup
1. Robot Operating System (ROS) under Ubuntu Operating System (test with Ubuntu 18 with ROS melodic)
2. Python 3 with these modules
	- pyglet
	- pyopengl
	- numpy
	- opencv
	- bezier
	- networkx
	- rospkg

Note that these modules can be installed via the command:
```bash
pip install <module(s)>
```
Alternatively, you can download the given requirement file (requirements.txt) and use:
```bash
pip install -r requirements.txt
```

3. Download and extract NeuroVis folder from [gitlab](https://gitlab.com/BRAIN_Lab/Biorobotics/vlib/tools/neurovis.git)
4. To test whether the installation is successful or not, first start a roscore in one terminal:
```bash
roscore
```
5. Navigate to your project ("neurovis"), then to "neurovis" folder. Inside the folder, you will find "src" subfolder (and "build" and "devel" subfolders, if you have built it before). If you have yet built the program, use:
In another one:
```bash
catkin_make
```
6. Next, use another command at add the program path to system path (you have do this every time you open the terminal). Alternatively, to make your computer run this command automatically after open new terminal, you can add this command to .bashrc
```bash
source <path_to_your_project>/neurovis/devel/setup.bash
```
or
```bash
nano ~/.bashrc
```
and open new terminal

7. Run the neurovis node using rosrun with
```bash
rosrun neurovis main.py
```

8. There are two example in "neurovis/examples". To run the examples use roslaunch command:
```bash
roslaunch neurovis_example1 neurovis_example1.launch
```
or
```bash
roslaunch neurovis_example2 neurovis_example2.launch
```
further information about the examples, please refer to [this page](neurovis/src) for more information.


## Example1

This ros node consists of a neural controller with one input neuron, one CPG, and one VRN. The neural controller is based on the ANN-framework. There are two inputbto the system: modulartory input (MI), which relates to frequency, and amplification/multiplication gain which affects the output signal amplitude. When MI changes the synaptics strength (thickness) of the connections between neuron C1 and C2 also changes accordingly. Initially, the connections is defined by the message from /neurovis/connections solely (This take more computational time since the whole connections might be reassigned). After a while, there is only weight updating (synaptic weights changes only, no reassignation); therefore, the message from /neurovis/weightUpdating is used instread. The normalization parameters are calculated automatically with the aim to normalize all the activations to be between zero (black) and one (green).


<div align="center">
<a href="https://drive.google.com/file/d/1frCkqumZBkLj9DIGvQ7n31sSKjCU4fdx/view?usp=sharing">
         <img alt="click" src="images/neurovis_example1.gif">
</a>
<p> Example1. (Click the picture and see the video.) </p>
</div>


## Example2

This ros node consists of a neural controller with one input neuron, one CPG, and one VRN. The neural controller is not based on the ANN-framework. There are two input to the system: modulartory input (MI), which relates to frequency, and amplification/multiplication gain which affects the output signal amplitude. When MI changes the synaptics strength (thickness) of the connections between neuron C1 and C2 also changes accordingly. Initially, the connections is defined by the message from /neurovis/connections solely (This take more computational time since the whole connections might be reassigned). After a while, there is only weight updating (synaptic weights changes only, no reassignation); therefore, the message from /neurovis/weightUpdating is use instread. The normalization parameters are calculated automatically with the aim to normalize all the activations to be between zero (black) and one (green). The major difference between example 2 and example 1 are (1) the program without relaying on ANN-framework and (2) the simplification of the VRN (seven-neuron network is simplified to a three-neuron network instead, two input neurons and one output neuron).


<div align="center">
<a href="https://drive.google.com/file/d/1Iw1kcIYw4er-1iX5Iy5R4jaQy1S9srFi/view?usp=sharing">
         <img alt="Qries" src="images/neurovis_example2.gif">
</a>
<p> Example2. (Click the picture and see the video.) </p>
</div>

## Input Messages

There are six inputs to the program: neuron name, connection, updated weight, neuron activity, normalization gain and bias. To use the visualizer, you have to publish ROS messages representing these. Some can be done once (i.e., neuron name), while others are done every timestep (i.e., neuron activity). It is emphasized that dimensions of all these should match. The summary is presented in the table below, where $n$ denotes the number of neurons and $n_w$ denotes the number of updated weights. 


| **ros message**  | **abbrivation** | **topic name**  | **size** | **datatype**|
| ---------- | :-------------: | ------------- | :------: | --------- |
| **neuron name** | $`N`$ | \neurovis\neuronName | $`n`$ | string (e.g. "N1/N2/N3/N4")| 
|**connection**|	$`\vec{C}`$	|\neurovis\connection|	$`n^2`$|	float32multiarray|
|**updated weight**|	$`\vec{W}`$	|\neurovis\weightUpdating|	$`3n_w`$|	float32multiarray (e.g. [pre,post,weight])|
|**neuron activity**|	$`\vec{A}`$|	\neurovis\neuronActivity|	$`n`$	|float32multiarray|
|**normalization gain**|	$`\vec{G}`$|	\neurovis\normalizationGain|	$`n`$	|float32multiarray|
|**normalization bias**|	$`\vec{B}`$|	\neurovis\normalizationBias|	$`n`$	|float32multiarray|

<div align="center">
<p> Table displaying ros messages and their details </p>
</div>

Figure below shows the result from running “rqt_graph” on a tested environment.
```bash
rqt_graph
```

<div align="center">
<img src="images/rqt_result.jpg" >
<p> The result of rqt_graph </p>
</div>

### Neuron Name (N)
Neuron name (N) is a string in the format of "N1/N2/.../Ni/.../Nn " where Ni denotes the name of neuron i. “/” is used to separate each neuron name. The message is a string message published via the topic named `\neurovis\neuronName` to `NeuroVis` node. This message determines how many neurons in your neural network. Note that the neuron can only be added and cannot be deleted (the unused neuron’s activity should be set to zero instead). 

### Connection ($`\vec{C}`$)
Connection is represented as a connection matrix. Connection matrix is a matrix of size $`n×n`$ that each element ($`w_{ij}`$) denotes the synaptic weight of the connection from neuron $`i`$ to neuron $`j`$. $`w_{ij}`$ can be either positive, negative, or zero (no connection). n is the number of neurons. 

``` math
C_{matrix} = \begin{bmatrix}
w_{00} & w_{01}  &  \ldots  & w_{0n} \\ 
w_{10} & w_{11} &  \ldots & w_{1n}\\ 
 \vdots & \vdots & \vdots & \vdots\\ 
 w_{n0}&  w_{n1} &  \ldots  & w_{nn} 
\end{bmatrix}
```

The connection matrix must be flattened before publishing. The flattened connection matrix ($`\vec{C}`$) is a vector/list of size $`n^2`$. It is published as float32multiarray via the topic named `\neurovis\connection` to `NeuroVis` node.

``` math
\vec{C} = \begin{bmatrix}
w_{00} \\ 
w_{01} \\ 
 \vdots \\ 
 w_{nn}
\end{bmatrix}
```

### Updated Weight ($`\vec{W}`$)
Updated weight ($`\vec{W}`$) is a vector/list of size $`3n_w`$ where $`n_w`$ denotes the number of updated weights.

``` math
\vec{W} = \begin{bmatrix}
Pre_{0} \\ 
Post_{0} \\
Weight_{0} \\ 
Pre_{1} \\ 
 \vdots \\ 
Weight_{n}
\end{bmatrix}
```

The message is a float32multiarray published via the topic named `\neurovis\weightUpdating` to `NeuroVis` node. This message determines the synaptic weights of several connection that you want to changes. Each updated connection (connection $`i`$) is defined by three values: $`Pre_{i}`$ denotes index of pre synaptics neuron, $`Post_{i}`$ denotes index of post synaptics neuron, and $`Weight_{i}`$ denotes the updated weight.


### Neuron Activity ($`\vec{A}`$)
Neuron activity ($`\vec{A}`$) is a vector/list of size $`n`$ where $`A_i`$ denotes the activity of neuron $`i`$. 

``` math
\vec{A} = \begin{bmatrix}
A_{0} \\ 
A_{1} \\ 
 \vdots \\ 
 A_{n}
\end{bmatrix}
```

The message is a float32multiarray published via the topic named `\neurovis\neuronActivity` to `NeuroVis` node. This message determines the neurons’ activity or the intensity of the neurons and their connection. Note that, before the visualization is displayed, the activity is normalized by the gain and bias according to the following equation where $`\vec{A'}`$ denotes normalized activity, $`\vec{G}`$ denotes the normalization gain and $`\vec{B}`$ denotes the normalization bias. 

``` math
\vec{A'} = \vec{G} \vec{A} + \vec{B}
```

### Normalization Gain ($`\vec{G}`$)
Normalization gain ($`\vec{G}`$) is a vector/list of size $`n`$ where $`G_i`$ denotes the normalization gain of neuron $`i`$. The message is a float32multiarray published via the topic named `\neurovis\normalizationGain` to `NeuroVis` node. 

``` math
\vec{G} = \begin{bmatrix}
G_{0} \\ 
G_{1} \\ 
 \vdots \\ 
 G_{n}
\end{bmatrix}
```

### Normalization Bias ($`\vec{B}`$)
Normalization gain ($`\vec{B}`$) is a vector/list of size $`n`$ where $`B_i`$ denotes the normalization gain of neuron $`i`$. The message is a float32multiarray published via the topic named `\neurovis\normalizationBias` to `NeuroVis` node. 

``` math
\vec{B} = \begin{bmatrix}
B_{0} \\ 
B_{1} \\ 
 \vdots \\ 
 B_{n}
\end{bmatrix}
```

Neuron name, normalization gain, and normalization bias can be published only one time at the start, while flatten connection matrix can be published only when the connection is updated. In contrast to these, neuron activity should be publish rapidily (every iterations).

## Buttons
There are four buttons on the top right: (1) saving configuration, (2) restoring configuration, (3) switching to using image as background, (4) recording video, and (5) switching theme (describe from top to bottom). 

With the first button, the network configuration (layout and connection parameters) is saved as a text file named “configuration.txt”. The second button initiates restoration. The third button switch the background between solid background and background with an image named “bg.png” locating in “images” folder. Therefore, you can customize your background image by replacing this image with the one you designed. When the fourth button in pressed, the visualization is recorded. The video is saved as “video.mp4” when the button is undressed (the process might take time). Finally, the lase button switch the theme of the visualizer between dark theme and light theme.

<div align="center">
<img src="images/buttons.jpg" >
<p> The buttons </p>
</div>

## Mouse Operations
1. Use the left button to select a neuron or connection's control point (arrow) and adjust the position by moving to the desired position. Click again to release. 
2. Use the roller to zoom in and out. 
3. Use both of the left and right button to adjust the display position (move the screen).


## ROS parameters
There are 22 ROS parameters as presented in the table below. The parameters are for configuration of the visualization such as the size of the neuron, type of layout and neuron outline. The parameters can be set manually by using:

``` bash
“rosparam set <parameter> <value>”
```

| **ros parameter**  | **description** | **type**  | **default** |
| ---------- | ------------- | ------------- | ------ |
|**\neurovis\init_neuron_num**	|neuron number (can be increase layer on by publishing new N)	|int|	1|
|**\neurovis\change_layout**	|allow changing new layout when new C is received or not|	boolean	|false|
|**\neurovis\reset_connection**| parameter	reset configuration of the connection when new C is received or not |	boolean	|false|
|**\neurovis\neuron_size**|	size of the neuron	|int|	30|
|**\neurovis\neuronName**|	display neuron name or not	|boolean|	false|
|**\neurovis\neuron_edge**|	number of edge for neuron	|int	|8|
|**\neurovis\neuron_outline**|	thickness of the outline	|int	|0|
|**\neurovis\num_bezier**|	number of the intermediate points of the connection|	int	|10|
|**\neurovis\connection_width**|	minimum, maximum width of the connection and multiplication gain|	list	|[0,15,8]|
|**\neurovis\arrow_size**|	size of the arrow	|int|	30|
|**\neurovis\recurrent_size**|	width of the recurrent connection|int	|100|
|**\neurovis\connection_opacity**|	maximum opacity of the connection|	float|	0.8|
|**\neurovis\layout**|	type of the layout (choose between kawai, spring, circular, or spectral)|	string|	“kawai”|
|**\neurovis\distribution**	|distribution factor (large = sparse)	|int|	100|
|**\neurovis\inactive_color**|	color of inactive connection/neuron in gray scale|	float	|0.1|
|**\neurovis\bg_opacity**	|background image opacity	|float	|0.5|
|**\neurovis\verbose**	|print display frequency in fps or not	|boolean|	true|
|**\neurovis\scroll_gain**|	how fast scrolling is	|float|	1.5|
|**\neurovis\button_size**|	size of the button|	int	|50|
|**\neurovis\record_every**	|(when recording) frequency of the recording|	int|	1 (every frames)|
|**\neurovis\video_fps**	|display frequency of saved video|	int|	20 (fps)|


<div align="center">
<p> Table displaying ros parameters and their details </p>
</div>

## Examples

There are currently two examples locating in the example folder. For more detail, please refers to another README.md file in that folder.

## Licenses

Create by Arthicha Srisuchinnawong 

E-mail : zumoarthicha@gmail.com

Update : 14 Sep 2020

GNU-v3 Copyright © 2020 Bio-Inspired Robotics & Neural Engineering laboratory (BRAIN-LAB), Vidyasirimedhi Institute of Science and Technology (VISTEC)
