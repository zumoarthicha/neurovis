# CMake generated Testfile for 
# Source directory: /home/zubuntu/Projects/neurovis/neurovis/src
# Build directory: /home/zubuntu/Projects/neurovis/neurovis/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("neurovis")
subdirs("neurovis_example1")
subdirs("neurovis_example2")
