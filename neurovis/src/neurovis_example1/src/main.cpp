// --------------------------------------------------------------------------------------
//       an example of a ann-framework-based ros node for interfacing neurovis
// --------------------------------------------------------------------------------------
// 
// 		This ros node consists of a neural controller with one input neuron, one CPG, 
// and one VRN. The neural controller is based on the ANN-framework. There are two input
// to the system: modulartory input (MI), which relates to frequency, and amplification/
// multiplication gain which affects the output signal amplitude. When MI changes the 
// synaptics strength (thickness) of the connections between neuron C1 and C2 also changes
// accordingly. Initially, the connections is defined by the message from /neurovis/conne-
// ctions solely (This take more computational time since the whole connections might be
// reassigned). After a while, there is only weight updating (synaptic weights changes 
// only, no reassignation); therefore, the message from /neurovis/weightUpdating is used
// instread. The normalization parameters are calculated automatically with the aim to 
// normalize all the activations to be between zero (black) and one (green). Enjoy!!
// --------------------------------------------------------------------------------------

// Standart libraries
#include <math.h>
#include <string>

// ROS-related libraries
#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/String.h"

// ANN framework
#include "utils/ann-framework/ann.h"
#include "utils/ann-framework/neuron.h"
#include "utils/ann-framework/synapse.h"

// Neural modules
#include "utils/ann-library/so2cpg.h"
#include "utils/ann-library/vrn.h"

// Neural Controller
#include "neural_controller.h"


class SO2CPG;
class VRN;
class NeuralController;



int main(int argc, char **argv)
{
	// --------------------- INITIALIZE NEURAL CONTROLLER ------------------------
	NeuralController controller(argc,argv);

    // -------------------------- INITIALIZE ROS -------------------------------
	ros::init(argc,argv,"neurovis_example1");
	ros::NodeHandle n;

	// initialize ros topics
	ros::Publisher name_pub = n.advertise<std_msgs::String>("/neurovis/neuronName", 1);
	ros::Publisher connection_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/connection", 1);
	ros::Publisher weightUpdating_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/weightUpdating", 1);
	ros::Publisher activity_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/neuronActivity", 1);
	ros::Publisher normGain_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/normalizationGain", 1);
	ros::Publisher normBias_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/normalizationBias", 1);

	// initialize data variable
	std_msgs::String name;
	std_msgs::Float32MultiArray connection;
	std_msgs::Float32MultiArray weightUpdating;
	std_msgs::Float32MultiArray activity;
    std_msgs::Float32MultiArray normGain;
    std_msgs::Float32MultiArray normBias;

	ros::Rate loop_rate(20);

	name.data = "V1/V2/V3/V4/V5/V6/V7/C1/C2/I1/";

	long count = 0;
	while(ros::ok())
	{
		// -------------------------- set neural control input -------------------------------
		switch(count){
			case 0:
				controller.cpg_MI = 0.01; controller.multiplicationGain=0.0;
				break;
			case 500:
				controller.cpg_MI = 0.01; controller.multiplicationGain=0.5;
				break;
			case 1000:
				controller.cpg_MI = 0.01; controller.multiplicationGain=1.0;
				break;
			case 1500:
				controller.cpg_MI = 0.1; controller.multiplicationGain=0.0;
				break;
			case 2000:
				controller.cpg_MI = 0.1; controller.multiplicationGain=0.5;
				break;
			case 2500:
				controller.cpg_MI = 0.1; controller.multiplicationGain=1.0;
				break;
			case 3000:
				controller.cpg_MI = 0.3; controller.multiplicationGain=0.0;
				break;
			case 3500:
				controller.cpg_MI = 0.3; controller.multiplicationGain=0.5;
				break;
			case 4000:
				controller.cpg_MI = 0.3; controller.multiplicationGain=1.0;
				break;
			case 4500:
				controller.cpg_MI = 0.01; controller.multiplicationGain=0.0;
				count = 200;
				break;
		}
		count += 1;
		
		// -------------------------- update the neural controller  -------------------------------
		controller.updateController();

		// -------------------------- get neural activity  -------------------------------
		activity = controller.getActivity();
		normGain = controller.getNormGain(); // to normalize neural activity (online)
		normBias = controller.getNormBias(); // to normalize neural activity (online)

		// -------------------------- publish message to neurovis  -------------------------------
		name_pub.publish(name);
		activity_pub.publish(activity);
		normGain_pub.publish(normGain);
		normBias_pub.publish(normBias);

		if (count < 200){
			// publish connection matrix (all weights)
			connection = controller.getConnection();
			connection_pub.publish(connection);
		}else{
			// publish only some weights (that change)
			weightUpdating.data.clear();
			weightUpdating.data.push_back((float)7); weightUpdating.data.push_back((float)8); weightUpdating.data.push_back((float) (0.18 + controller.cpg_MI));  
			weightUpdating.data.push_back((float)8); weightUpdating.data.push_back((float)7); weightUpdating.data.push_back((float) (-0.18 - controller.cpg_MI));  
			weightUpdating_pub.publish(weightUpdating);
		}

		ros::spinOnce();
		loop_rate.sleep();
	}
}