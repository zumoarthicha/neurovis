#ifndef NEURAL_CONTROLLER_H
#define NEURAL_CONTROLLER_H

// Standart libraries
#include <math.h>
#include <string>

// ROS-related libraries
#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/String.h"

// ANN framework
#include "utils/ann-framework/ann.h"
#include "utils/ann-framework/neuron.h"
#include "utils/ann-framework/synapse.h"

// Neural modules
#include "utils/ann-library/so2cpg.h"
#include "utils/ann-library/vrn.h"

using namespace std;


class SO2CPG;
class VRN;

class NeuralController : public ANN {
public:
    float cpg_MI = 0.01;
    float multiplicationGain = 0.5;

    NeuralController(int argc, char *argv[]);
    void updateController();
    std_msgs::Float32MultiArray getConnection();
    std_msgs::Float32MultiArray getActivity();
    std_msgs::Float32MultiArray getNormGain();
    std_msgs::Float32MultiArray getNormBias();

private:

    vector<Neuron*> inputNeurons;
    vector<Neuron*> Neurons;
    vector<float> actmin;
    vector<float> actmax;

    
    SO2CPG * cpg; // central pattern generator
    VRN * vrn; // velocity regulating network

    std_msgs::String name;
    std_msgs::Float32MultiArray connection;
    std_msgs::Float32MultiArray weightUpdating;
    std_msgs::Float32MultiArray activity;
    std_msgs::Float32MultiArray normGain;
    std_msgs::Float32MultiArray normBias;

    
};

#endif //NEURAL_CONTROLLER_H
