#include "neural_controller.h"
using namespace std;

NeuralController::NeuralController(int argc, char *argv[]) {
    // --------------------- INITIALIZE NEURAL CONTROLLER ------------------------
    // initialize CPG
    cpg = new SO2CPG();
    addSubnet(cpg);
    cpg->setOutput(1, 0.885); cpg->setOutput(0, -0.59268);
    cpg->setActivity(0, 0.885); cpg->setActivity(1, -0.59268);
    cpg->setWeight(0, 0, 1.4); cpg->setWeight(1, 1, 1.4);
    cpg->setWeight(0, 1, 0.18 + 0.1); cpg->setWeight(1, 0, -0.18 - 0.1);
    cpg->setBias(0, 0.0); cpg->setBias(1, 0.0);

    // initialize an input neurons, which is for VRN 
    inputNeurons.push_back(addNeuron());
    inputNeurons.at(0)->setTransferFunction(identityFunction());

    // initialize vrn
    vrn = new VRN();
    addSubnet(vrn);
    w(vrn->getNeuron(0), cpg->getNeuron(0), 1.0);
    w(vrn->getNeuron(1), inputNeurons[0], 1.0); 

    Neurons = getAllNeurons();
    for (int i=0;i<Neurons.size();i++)
    {
        actmin.push_back(1e10);
        actmax.push_back(-1.0*1e10);

        normGain.data.push_back(1.0);
        normBias.data.push_back(0.0);
    }
} // constructor

void NeuralController::updateController() {
    setInput(inputNeurons[0],multiplicationGain);
    cpg->setWeight(0, 1, 0.18 + cpg_MI); cpg->setWeight(1, 0, -0.18 - cpg_MI);
    updateActivities();
    updateOutputs();
}//NeuralController::runController()

std_msgs::Float32MultiArray NeuralController::getConnection()
{
    Neurons = getAllNeurons();
    connection.data.clear();    
    float Cmatrix[Neurons.size()*Neurons.size()] = {0};

    for (int i=0; i< Neurons.size(); i++){for (int j=0; j< Neurons.size(); j++){
        Synapse * s = Neurons[i]->getSynapseFrom(Neurons[j]);
        if(s){Cmatrix[Neurons.size()*j+i] += s->getWeight();}
    }}
    for (int i =0; i<(Neurons.size()*Neurons.size());i++){connection.data.push_back(Cmatrix[i]);}
    
    return connection;
}

std_msgs::Float32MultiArray NeuralController::getActivity()
{
    Neurons = getAllNeurons();
    activity.data.clear();
    for (int i=0; i< Neurons.size(); i++){
        float output = Neurons[i]->getOutput();
        activity.data.push_back(output);

        if(output < actmin[i]){actmin[i]=output;}
        if(output > actmax[i]){actmax[i]=output;}
    }
    return activity;
}

std_msgs::Float32MultiArray NeuralController::getNormGain()
{
    for(int i =0 ; i < actmin.size(); i++)
    {
        if (actmax[i] != actmin[i]){normGain.data[i] = 1.0/(actmax[i]-actmin[i]);}
    }
    return normGain;
}

std_msgs::Float32MultiArray NeuralController::getNormBias()
{
    for(int i =0 ; i < actmin.size(); i++)
    {
        if (actmax[i] != actmin[i]){normBias.data[i] = -1.0*actmin[i]/(actmax[i]-actmin[i]);}
    }
    return normBias;
}

