// --------------------------------------------------------------------------------------
//   an example of a ros node creating without ANN-framework for interfacing neurovis
// --------------------------------------------------------------------------------------
// 
// 		This ros node consists of a neural controller with one input neuron, one CPG, 
// and one VRN. The neural controller is not based on the ANN-framework. There are two input
// to the system: modulartory input (MI), which relates to frequency, and amplification/
// multiplication gain which affects the output signal amplitude. When MI changes the 
// synaptics strength (thickness) of the connections between neuron C1 and C2 also changes
// accordingly. Initially, the connections is defined by the message from /neurovis/conne-
// ctions solely (This take more computational time since the whole connections might be
// reassigned). After a while, there is only weight updating (synaptic weights changes 
// only, no reassignation); therefore, the message from /neurovis/weightUpdating is used
// instread. The normalization parameters are calculated automatically with the aim to 
// normalize all the activations to be between zero (black) and one (green). The major 
// difference between example 2 and example 1 are (1) the program without relaying on 
// ANN-framework and (2) the simplification of the VRN (seven-neuron network is simplified
// to a three-neuron network instead, two input neurons and one output neuron). Enjoy!!
// --------------------------------------------------------------------------------------

// Standart libraries
#include <cmath>
#include <string>

// ROS-related libraries
#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/String.h"
using namespace std;

float connections[6*6] = {0}; // flatten connection matrix

double sigmoidd(double x); // just a simple sigmoid function
void setConnection(int i, int j, float w); // set a specific weight in the flatten connection matrix


int main(int argc, char **argv)
{
	// --------------------- INITIALIZE NEURAL CONTROLLER ------------------------
	double cpg[2] = {0.885,-0.59268};
	double cpg_act[2] = {0.0,0.0};
	double input[1] = {0.5};
	double vrn[7] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};
	double actmin[10] = {1e10};
	double actmax[10] = {-1e10};
	
	// input/feedback
	double cpg_MI = 0.01;
	double multiplicationGain = 0.5;

    // -------------------------- INITIALIZE ROS -------------------------------
	ros::init(argc,argv,"neurovis_example2");
	ros::NodeHandle n;

	// initialize ros topics
	ros::Publisher name_pub = n.advertise<std_msgs::String>("/neurovis/neuronName", 1);
	ros::Publisher connection_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/connection", 1);
	ros::Publisher weightUpdating_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/weightUpdating", 1);
	ros::Publisher activity_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/neuronActivity", 1);
	ros::Publisher normGain_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/normalizationGain", 1);
	ros::Publisher normBias_pub = n.advertise<std_msgs::Float32MultiArray>("/neurovis/normalizationBias", 1);

	// initialize data variable
	std_msgs::String name;
	std_msgs::Float32MultiArray connection;
	std_msgs::Float32MultiArray weightUpdating;
	std_msgs::Float32MultiArray activity;
    std_msgs::Float32MultiArray normGain;
    std_msgs::Float32MultiArray normBias;

    for (int i=0;i<6;i++)
    {
        normGain.data.push_back(1.0);
        normBias.data.push_back(0.0);
    }

	ros::Rate loop_rate(20);

	name.data = "C1/C2/I1/V1/V2/V3/";

	long count = 0;
	while(ros::ok())
	{
		// -------------------------- set neural control input -------------------------------
		switch(count){
			case 0:
				cpg_MI = 0.01; multiplicationGain=0.0;
				break;
			case 400:
				cpg_MI = 0.01; multiplicationGain=0.2;
				break;
			case 800:
				cpg_MI = 0.01; multiplicationGain=1.0;
				break;
			case 1200:
				cpg_MI = 0.1; multiplicationGain=0.0;
				break;
			case 1600:
				cpg_MI = 0.1; multiplicationGain=0.2;
				break;
			case 2000:
				cpg_MI = 0.1; multiplicationGain=1.0;
				break;
			case 2400:
				cpg_MI = 0.3; multiplicationGain=0.0;
				break;
			case 2800:
				cpg_MI = 0.3; multiplicationGain=0.2;
				break;
			case 3200:
				cpg_MI = 0.3; multiplicationGain=1.0;
				break;
			case 3600:
				cpg_MI = 0.01; multiplicationGain=0.0;
				count = 200;
				break;
		}
		count += 1;
		
		// -------------------------- update the neural controller  -------------------------------
		// cpg
		cpg_act[0] = 1.4*cpg[0]+(0.18+cpg_MI)*cpg[1];
		cpg_act[1] = 1.4*cpg[1]-(0.18+cpg_MI)*cpg[0];
		cpg[0] = tanh(cpg_act[0]);
		cpg[1] = tanh(cpg_act[1]);

		// input neuron(s)
		input[0] = multiplicationGain;

		// vrn
		vrn[0] = cpg[0];
		vrn[1] = input[0];
		vrn[2] = sigmoidd((0.000001*vrn[0])+(0.000001*vrn[1])+2.3787);
		vrn[3] = sigmoidd((-1e-6*vrn[0])-(1e-6*vrn[1])+2.3787);
		vrn[4] = sigmoidd((1e-6*vrn[0])-(1e-6*vrn[1])+2.3787);
		vrn[5] = sigmoidd((-1e-6*vrn[0])+(1e-6*vrn[1])+2.3787);
		vrn[6] = (-3.87882069e+12*vrn[2])-(3.87882069e+12*vrn[3])+(3.87882069e+12*vrn[4])+(3.87882069e+12*vrn[5]);
		

		// -------------------------- get neural activity  -------------------------------
		// neuron activities
		activity.data.clear();
		for(int i=0;i<2;i++){activity.data.push_back(cpg[i]);}
		for(int i=0;i<1;i++){activity.data.push_back(input[i]);}
		activity.data.push_back(vrn[0]);
		activity.data.push_back(vrn[1]);
		activity.data.push_back(vrn[6]);

		// -------------------------- calculate normalization gain  -------------------------------
		for(int i=0;i<6;i++)
		{
			if(activity.data[i] < actmin[i]){actmin[i]=activity.data[i];}
        	if(activity.data[i] > actmax[i]){actmax[i]=activity.data[i];}

        	// normalization parameters
        	if (actmax[i] != actmin[i]){normGain.data[i] = 1.0/(actmax[i]-actmin[i]);}else{normGain.data[i] = 1.0;}
        	if (actmax[i] != actmin[i]){normBias.data[i] = -1.0*actmin[i]/(actmax[i]-actmin[i]);}else{normBias.data[i]=0.0;}
		}
		
		// -------------------------- publish message to neurovis  -------------------------------
		name_pub.publish(name);
		activity_pub.publish(activity);
		normGain_pub.publish(normGain);
		normBias_pub.publish(normBias);

		if(count < 200){
			// publish connection matrix (all weights)
			// connections
			setConnection(0,0,1.4); setConnection(1,1,1.4);
			setConnection(0,1,-0.18-cpg_MI);setConnection(1,0,0.18+cpg_MI);
			setConnection(0,3,1.0); setConnection(2,4,1.0);
			setConnection(3,5,1.0); setConnection(4,5,1.0);

			connection.data.clear();
			for(int i=0;i<(6*6);i++){connection.data.push_back(connections[i]);}
			connection_pub.publish(connection);
		}
		else{
			// publish only some weights (that change)
			// weight updating
			weightUpdating.data.clear();
			weightUpdating.data.push_back((float)0); weightUpdating.data.push_back((float)1); weightUpdating.data.push_back((float) (-0.18 - cpg_MI));  
			weightUpdating.data.push_back((float)1); weightUpdating.data.push_back((float)0); weightUpdating.data.push_back((float) (0.18 + cpg_MI));  
			weightUpdating_pub.publish(weightUpdating);
		}

		ros::spinOnce();
		loop_rate.sleep();
	}
}


double sigmoidd(double x){
	return 1./(1.+std::exp(-x));
}

void setConnection(int i, int j, float w){
	connections[6*i+j] = w;
}