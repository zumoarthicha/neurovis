# **Example**

There are currently two examples here. The first one is an example of the controller created using ANN-framework, while the second one is without the library.

## Directory Tree

```
├── CMakeLists.txt
├── neurovis
│   ├── CMakeLists.txt
│   ├── configurations
│   │   ├── example1.txt
│   │   └── example2.txt
│   ├── CrimsonPro.ttf
│   ├── images
│   │   ├── bg.png
│   │   ├── bt0.png
│   │   ├── bt1.png
│   │   ├── bt2.png
│   │   ├── bt2_pressed.png
│   │   ├── bt3.png
│   │   ├── bt3_pressed.png
│   │   ├── bt4.png
│   │   └── bt4_pressed.png
│   ├── launch
│   ├── main.py
│   ├── NeuroVisNode.py
│   ├── NeuroVis.py
│   ├── NeuroVis.pyc
│   ├── package.xml
│   └── __pycache__
│       └── NeuroVis.cpython-36.pyc
├── neurovis_example1
│   ├── bin
│   │   └── neurovis_example1
│   ├── CMakeLists.txt
│   ├── launch
│   │   └── neurovis_example1.launch
│   ├── package.xml
│   └── src
│       ├── main.cpp
│       ├── neural_controller.cpp
│       ├── neural_controller.h
│       └── utils
├── neurovis_example2
│   ├── bin
│   │   └── neurovis_example2
│   ├── CMakeLists.txt
│   ├── launch
│   │   └── neurovis_example2.launch
│   ├── package.xml
│   └── src
│       └── main.cpp
└── README.md
```
Note that all the source codes are in ```neurovis_example1/src``` and ```neurovis_example2/src```, and the configuration file (configuration.txt) are at ```neurovis/configurations```.
