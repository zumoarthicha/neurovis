#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32MultiArray, String
import numpy as np
import NeuroVis
from math import sqrt
import sys, threading
import os, subprocess

print("start")
rospy.init_node('NeuroVis')
rate = rospy.Rate(60)

init_neuron_num = rospy.get_param("/neurovis/init_neuron_num",1)
change_layout = rospy.get_param("/neurovis/change_layout",True)
reset_connection_parameter = rospy.get_param("/neurovis/reset_connection_parameter",True)

isrunning = True
viz = None

	

def cmatCB(msg):
	global viz, change_layout, reset_connection_parameter
	n = int(sqrt(len(msg.data)))
	C = np.array(msg.data).reshape(n,n)
	viz.set_connection(C.tolist(),change_layout=change_layout,reset_connection_parameter=reset_connection_parameter)

def actCB(msg):
	global viz
	try:
		viz.set_activity(msg.data)
	except:
		pass

def weightUpdatingCB(msg):
	global viz
	try:
		if(len(msg.data)%3 == 0):
			for i in range(0,len(msg.data),3):
				viz.update_weight(int(msg.data[i]),int(msg.data[i+1]),float(msg.data[i+2]))
	except:
		pass

def debugCB(msg):
	print('dbg',msg.data)

def nameCB(msg):
	global viz, name_done
	N = msg.data.split("/")[:-1]
	try:
		viz.set_neuron_name(N)
	except:
		pass

def gainCB(msg):
	global viz
	try:
		viz.set_normalization_gain(msg.data)
	except:
		pass

def biasCB(msg):
	global viz
	try:
		viz.set_normalization_bias(msg.data)
	except:
		pass

def ros_thread():
	global rate, viz, apb
	while 1:#not rospy.is_shutdown():
		update_parameters()
		rate.sleep()
		if not isrunning:
			sys.exit()
	
def update_parameters():
	global viz, change_layout, reset_connection_parameter
	if viz is not None:

		confpath = rospy.get_param("/neurovis/configuration_path",None)
		change_layout = rospy.get_param("/neurovis/change_layout",False)
		reset_connection_parameter = rospy.get_param("/neurovis/reset_connection_parameter",False)

		neuron_size = rospy.get_param("/neurovis/neuron_size",30)
		neuron_name = rospy.get_param("/neurovis/neuron_name",True)
		neuron_edge = rospy.get_param("/neurovis/neuron_edge",8)
		neuron_outline = rospy.get_param("/neurovis/neuron_outline",0)

		num_bezier = rospy.get_param("/neurovis/num_bezier",10)
		connection_width = rospy.get_param("/neurovis/connection_width",[0,15,8])
		arrow_size = rospy.get_param("/neurovis/arrow_size",30)
		recurrent_size = rospy.get_param("/neurovis/recurrent_size",100)
		connection_opacity = rospy.get_param("/neurovis/connection_opacity",0.8)

		layout = rospy.get_param("/neurovis/layout",'kawai')
		distribution = rospy.get_param("/neurovis/distribution",100)

		inactive_color = rospy.get_param("/neurovis/inactive_color",0.1)
		bg_opacity = rospy.get_param("/neurovis/bg_opacity",0.4)

		verbose = rospy.get_param("/neurovis/verbose",True)
		scroll_gain = rospy.get_param("/neurovis/scroll_gain",1.5)
		button_size = rospy.get_param("/neurovis/button_size",50)
		record_every = rospy.get_param("/neurovis/record_every",1)
		video_fps = rospy.get_param("/neurovis/video_fps",5)


		viz.NEURONSIZE = neuron_size # size of the neurons
		viz.NEURONNAME = neuron_name # enable or disable neuron name
		viz.NCIRCLEEDGES = neuron_edge # number of edges of circle (neuron)
		viz.OUTLINETHICKNESS = neuron_outline # size of outline thickness (>= 0 means no outline)

		''' (O) Connections '''
		viz.NBEZIERPOINTS = num_bezier # number of bazier points/sections
		viz.CONNECTIONWIDTH = connection_width # range of connection width [minimum, maximum, multiplication gain]
		viz.ARROW_SIZE = arrow_size # size of the arrow
		viz.RECURRENTRADIUS = recurrent_size # radious of recurrent connection
		viz.MAXCONNECTIONALPHA = connection_opacity # maximum connection transparantcy

		''' (O) Layout '''
		viz.LAYOUT = layout # kawai, spring, circular, spectral
		viz.DISTRIBUTIONFACTOR = distribution # distribution factor, i.e. distance between each node initially created

		''' (O) Color '''
		viz.INACTIVATECOLOR = inactive_color # intensity of inactivate connections/neurons
		viz.BGOPACITY = bg_opacity
		
		''' (O) Control '''
		viz.VERBOSE = verbose # print fps or not
		viz.SCROLLGAIN = scroll_gain # control how fast zooming is
		viz.BUTTONSIZE = button_size # size of the buttons
		viz.RECORDFREQUENCY = record_every # record every n frames
		viz.VIDEOFPS = video_fps # display speed of the video (fps)

		viz.set_confpath(confpath)

if 1:


	a = subprocess.check_output(['rostopic', 'list'])
	while(not "/neurovis/neuronName" in a.decode("utf-8")):
		a = subprocess.check_output(['rostopic', 'list'])
	
	rospy.Subscriber("/neurovis/connection", Float32MultiArray, cmatCB) # connection matrix
	rospy.Subscriber("/neurovis/weightUpdating", Float32MultiArray, weightUpdatingCB) # alternative method for weight updating
	rospy.Subscriber("/neurovis/neuronActivity", Float32MultiArray, actCB,tcp_nodelay=True) # activity vector
	rospy.Subscriber("/neurovis/neuronName", String, nameCB) # name list
	rospy.Subscriber("/neurovis/normalizationGain", Float32MultiArray, gainCB) # activity vector
	rospy.Subscriber("/neurovis/normalizationBias", Float32MultiArray, biasCB) # name list

	
	# start ros thread
	thread  = threading.Thread(target=ros_thread)
	thread.start()

	dir_path = os.path.dirname(os.path.realpath(__file__))
	viz = NeuroVis.neurovis(init_neuron_num,path=dir_path+'/')
	viz.start()
	isrunning = False
