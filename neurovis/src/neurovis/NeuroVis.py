from pyglet.gl import *
from math import *
import numpy as np
import networkx as nx
import os, shutil, sys, copy, random, time, glob
import bezier, cv2
import time as pytime

''' ------------- defined value returned when click/scroll '''
LEFT = 1
SCROLL = 2
RIGHT = 4
BOTH = 5



class neurovis:
	'''
	---------------- neuro visualization class ---------------
	create neurovis visualizer and user interface
	The visualizer plots neurons and their connections with the corresponding parameters such as activity and weight,
	in real-time. It receives three mandatory inputs: (1) List of neuron names <ListOfString>, (2) Connection matrix 
	<ListOfFloat>, which each element is the synaptics strength/weight between two neurons, and (3) List of neuron 
	activity <ListOfFloat>. Adding neurons and changing connection weights are possible by senting a new sets of
	inputs to the visualizer. Clicked and dragged, the neuron/connection configuation can also be adjusted. With the 
	buttons on the left, user can save/load the configuration, changing background, record a video, and changing the
	theme of visualizer.
	'''

	''' ------------- private attributes ------------- '''
	__window = pyglet.window.Window(width=1280,height=720,resizable=True,caption='NeuroVis: neuro dynamic visualizer')
	
	__path = ''
	__confpath = ''

	__neurons = [] # vertices: [name, position (x,y) activity]
	__connections = [] # edges: [position (from,to), weight, control-points, arrow position]
	__previous_weight = [] # previous weight [weight1,weight2, ...]
	__activity_gain = [] # gain for normalizing neuron activity
	__activity_bias = [] # bias for normalizing neuron activity
	__buttons = [] # name, position, image/object
	__C_matrix = None # connection matrix
	__state = ['free',None] # state (e.g, move neurons) and state parameter (e.g., which neurons)

	__frame_counter = 0 # counter for displaying speed (fps)
	__numframe = 0 # counter for recording video
	__numdisplayedframe = 0 # counter for frames 
	__fps = 10 # display frequency
	__time_init = 0 # time counter
	__bgimage = None # state/image used as background
	__record_state = False # state for recording
	__button_names = ['save','recover','background','record','theme'] # button names and order

	__previous_windowsize = [1280,720] # window size (previous timestep)
	__bg = None # background image
	
	__camscale = 1.0#0.5 # zoom (1.0 = normal, <1.0 = zoom out, >1.0 = zoom in)
	__campos = [0,0]#[640,360] # display position

	# object
	__text = None
	__timecounter = 0


	''' ------------- confugurable parameter ---------------------- '''
	''' (O) Neurons '''
	NEURONNAME = True # enable or disable neuron name
	NEURONSIZE = 30.0 # size of the neurons
	NCIRCLEEDGES = 8 # number of edges of circle (neuron)
	OUTLINETHICKNESS = 0 # size of outline thickness (>= 0 means no outline)

	''' (O) Connections '''
	NBEZIERPOINTS = 10.0 # number of bazier points/sections
	CONNECTIONWIDTH = (0,20,10) # range of connection width [minimum, maximum, multiplication gain]
	ARROW_SIZE = 30.0 # size of the arrow
	RECURRENTRADIUS = 100 # radious of recurrent connection
	MAXCONNECTIONALPHA = 0.8 # maximum connection transparantcy
	GRADIENTMULTIPLIER = (1046,500) # multiplication gain for weight change [size, alpha]

	''' (O) Layout '''
	LAYOUT = 'kawai' # kawai, spring, circular, spectral
	DISTRIBUTIONFACTOR = 300*10 # distribution factor, i.e. distance between each node initially created

	''' (O) Color '''
	BGCOLOR = 0.0 # background color (0.0: black -1.0: white)
	INACTIVATECOLOR = 0.1 # intensity of inactivate connections/neurons
	BGOPACITY = 0.5
	
	''' (O) Control '''
	VERBOSE = True # print fps or not
	SCROLLGAIN = 1.5 # control how fast zooming is
	BUTTONSIZE = 50 # size of the buttons
	RECORDFREQUENCY = 1 # record every n frames
	VIDEOFPS = 10 # display speed of the video (fps)
	OMIT = True

	''' ------------- Methods ------------- '''
	def __init__(self,n_neurons,path=None):
		'''
		neurovis constructor: initiaization
		'''
		self.__window.set_mouse_visible(True)
		self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_DEFAULT))
		if path != None:
			os.chdir(os.path.dirname(path))
			mypath = os.path.dirname( os.path.realpath( __file__ ) )
			pyglet.resource.path = [mypath]
			pyglet.resource.reindex()
		else:
			self.__path = ''
		
		pyglet.font.add_file('CrimsonPro.ttf')
		self.__time_init = time.perf_counter()
		
		self.__C_matrix = np.zeros((n_neurons,n_neurons),dtype=np.float32).tolist()

		self.__neurons = []
		self.__connections = []
		for n in range(0,n_neurons):
			self.__neurons.append(['',[0.0,0.0],1.0])
		self.__activity_gain = (np.zeros((n_neurons),dtype=np.float32) + 1.0).tolist()
		self.__activity_bias = np.zeros((n_neurons),dtype=np.float32).tolist()

		# create events & initialize call back functions
		self.on_draw = self.__window.event(self.on_draw)
		self.on_mouse_scroll = self.__window.event(self.on_mouse_scroll)
		self.on_mouse_drag = self.__window.event(self.on_mouse_drag)
		self.on_mouse_press = self.__window.event(self.on_mouse_press)
		self.on_mouse_release = self.__window.event(self.on_mouse_release)
		self.on_mouse_motion = self.__window.event(self.on_mouse_motion)
		#pyglet.resource.path = [os.path.dirname(path)]
		
		for i in range(0,5):
			if 1:
				self.__buttons.append([str(i),[0,0],pyglet.resource.image('images/bt'+str(i)+'.png')])
				self.__buttons[i][2].width= self.BUTTONSIZE
				self.__buttons[i][2].height = self.BUTTONSIZE
		print('Complete initiaization')

	def set_confpath(self,confpath):
		self.__confpath = confpath if confpath != None else 'configuration.txt'

	def start(self):
		'''
		start visualizer, and the visualizer will be run until the program is closed
		'''

		pyglet.clock.schedule(self.update_frame)
		pyglet.app.run()

	def set_neuron_name(self,N):
		'''
		Update the neuron names
		Default (if this function have yet been called): '' for every neurons
		'''
		for i in range(0,len(N)):
			if i < len(self.__neurons):
				self.__neurons[i][0] = N[i]
			else:
				self.__neurons.append([N[i],[0.0,0.0],0.0])

	def set_connection(self,C,change_layout = False,reset_connection_parameter=False):
		'''
		Update connections using given C-matrix (connection matrix; with synaptic strength/weight)
		If "restore_position" is given (not None), some position are fixed to those.
		Currently, the visualization is not designned reducing neuron. However, adding neuron is
		possible by calling this function multiple time. The program will error if the dimension
		of the given neuron list (N) and C-matrix (C) is contradict. 
		'''
		delList = []
		if (len(self.__C_matrix) == len(C)) and (len(self.__C_matrix[0]) == len(C[0])):
			# in case no nueron are added

			self.__C_matrix = copy.deepcopy(C) # update C_matrix

			if change_layout: 
				# if restore position is not given, initialize new layout
				layout = self.__Cmatrix2layout(self.__C_matrix)
				gkeys = list(layout.keys())
				assert len(gkeys) <= len(self.__neurons)
				cx = 0
				cy = 0
				for i in range(0,len(gkeys)):
					self.__neurons[i][1] = list(0*np.array(list(layout[gkeys[i]]))*self.DISTRIBUTIONFACTOR) # position
				#self.__campos[1] = self.__neurons[-1][1][1]-self.__previous_windowsize[1]//2
				#self.__campos[0] = self.__neurons[-1][1][0]-self.__previous_windowsize[0]//2
			if reset_connection_parameter:
				self.__connections = []
				for i in range(0,len(C)):
					for j in range(0,len(C[i])):
						if C[i][j] != 0.0:
							self.__connections.append([0]*4)
							self.__connections[-1][0] = [i,j] # position (from, to)
							self.__connections[-1][1] = C[i][j] # weight
							self.__connections[-1][2] = self.__get_control_points(self.__neurons[i],self.__neurons[j],side=1) # control points
							self.__connections[-1][3] = [0,0] # arrow position
			else:
				for i in range(0,len(C)):
					for j in range(0,len(C[i])):
						if C[i][j] != 0.0:
							found = False
							for k in range(0,len(self.__connections)):
								if (i == self.__connections[k][0][0]) and (j == self.__connections[k][0][1]):
									self.__connections[k][1] = C[i][j]
									found = True
									break
							if not found:
								self.__connections.append([0]*4)
								self.__connections[-1][0] = [i,j] # position (from, to)
								self.__connections[-1][1] = C[i][j] # weight
								self.__connections[-1][2] = self.__get_control_points(self.__neurons[i],self.__neurons[j],side=1) # control points
								self.__connections[-1][3] = [0,0] # arrow position
						else:
							for k in range(0,len(self.__connections)):
								if (i == self.__connections[k][0][0]) and (j == self.__connections[k][0][1]):
									delList.append(k)
									break
				delList.sort(reverse = True)
				for i in delList:
					del self.__connections[i]


		else:
			difference = - len(self.__C_matrix) + len(C)
			# in case one or more neuron are added
			for i in range(0,len(C)): # expand C_matrix and update the existing connections
				if i < len(self.__C_matrix):
					for j in range(0,len(C[0])):
						if j < len(self.__C_matrix[i]):
							self.__C_matrix[i][j] = C[i][j]
						else:
							self.__C_matrix[i].append(C[i][j])
				else:
					temp = []
					for j in range(0,len(C[i])):
						temp.append(C[i][j])
					self.__C_matrix.append(temp)

			# update neuron positon by 
			# random the position of the added neuron if LAYER = 'kawai'
			# for other method let the algorithm decide the positon	
			neuron_pos = {}
			for i in range(0,len(self.__neurons)):
				if (self.__neurons[i][1][0] == 0) and (self.__neurons[i][1][1] == 0):
					neuron_pos[i] = (2.0*np.random.rand(2)-1.0).tolist()
				else:
					neuron_pos[i] = self.__neurons[i][1]

			layout = self.__Cmatrix2layout(self.__C_matrix,pos=neuron_pos)
			gkeys = list(layout.keys())
			assert len(gkeys) <= len(self.__neurons)
			for i in range(0,len(gkeys)):
				if (self.__neurons[i][1][0] == 0) and (self.__neurons[i][1][1] == 0):
					if self.LAYOUT is 'kawai':
						self.__neurons[i][1] = list((2.0*np.random.rand(2)-1.0)*self.DISTRIBUTIONFACTOR) # position
					else:
						self.__neurons[i][1] = list(np.array(list(layout[gkeys[i]]))*self.DISTRIBUTIONFACTOR) # position

			# update the existing elements in self.__connections and create the new ones
			for i in range(0,len(self.__C_matrix)):
				for j in range(0,len(self.__C_matrix[i])):
					if self.__C_matrix[i][j] != 0.0:
						found = False
						for k in range(0,len(self.__connections)):
							if self.__connections[k][0] == [i,j]:
								self.__connections[k][1] = self.__C_matrix[i][j] # weight
								found = True
								break
						if not found:
							self.__connections.append([0]*4)
							self.__connections[-1][0] = [i,j] # position (from, to)
							self.__connections[-1][1] = self.__C_matrix[i][j] # weight
							self.__connections[-1][2] = self.__get_control_points(self.__neurons[i],self.__neurons[j],side=1) # control points
							self.__connections[-1][3] = [0,0] # arrow position
			
			self.__activity_gain += (np.zeros((difference),dtype=np.float32) + 1.0).tolist()
			self.__activity_bias += np.zeros((difference),dtype=np.float32).tolist()

		self.__previous_weight = np.array(self.__connections)[:,1]

	def update_weight(self,pre,post,weight):
		for k in range(0,len(self.__connections)):
			if (self.__connections[k][0][0] == pre) and (self.__connections[k][0][1] == post):
				self.__connections[k][1] = weight


	def set_normalization_gain(self,gain):
		''' Update normalization parameter/ activity gains and biases
		Default (if this function have yet been called): 1.0 and 0.0 for every neurons
		'''
		# gain
		self.__activity_gain = (np.zeros((len(gain)),dtype=np.float32) + 1.0).tolist()
		if (type(gain) == type([''])) or (type(gain) == type(('',''))):
			if len(gain) == len(self.__neurons):
				for i in range(0,len(gain)):
					self.__activity_gain[i] = gain[i]
			else:
				print("Dimension mismatch:","normalization gain", len(gain),"neurons", len(self.__neurons))
		else:
			for i in range(0,len(gain)):
				self.__activity_gain[i] = gain

	def set_normalization_bias(self,bias):
		''' Update normalization parameter/ activity gains and biases
		Default (if this function have yet been called): 1.0 and 0.0 for every neurons
		'''
		# bias
		self.__activity_bias = np.zeros((len(bias)),dtype=np.float32).tolist()
		if (type(bias) == type([''])) or (type(bias) == type(('',''))):
			if len(bias) == len(self.__neurons):
				for i in range(0,len(bias)):
					self.__activity_bias[i] = bias[i]
			else:
				print("Dimension mismatch:","normalization bias", len(bias),"neurons", len(self.__neurons))
		else:
			for i in range(0,len(bias)):
				self.__activity_bias[i] = bias

	def set_activity(self,list_of_act):
		'''
		Update the activity of the neurons
		Note that length of "list_of_act" should be equal to number of neuron.
		Default (if this function have yet been called): 0 for every neurons
		'''
		if len(list_of_act) == len(self.__neurons):
			for i in range(0,len(list_of_act)):
				self.__neurons[i][2] = list_of_act[i]
		else:
			print("Dimension mismatch:","activity", len(list_of_act),"neurons", len(self.__neurons))

	''' --------------------- Methods for events -------------------- '''
	def on_mouse_scroll(self,x, y, dx, dy):
		'''
		Increase/decrease self.__camscale when scroll
		'''
		self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_SIZE_UP_DOWN))
		self.__camscale += 0.01*dy*self.SCROLLGAIN
		self.__campos[0] -= dy*self.SCROLLGAIN if self.__camscale > 0.05 else 0
		self.__campos[1] -= dy*self.SCROLLGAIN if self.__camscale > 0.05 else 0
		self.__camscale = max(self.__camscale,0.05)
		
	def on_mouse_drag(self,x, y, dx, dy, buttons, modifiers):
		'''
		Left click and drag: move a neuron or control point when drag neuron or arrow.
		Left and Right click and drag: move window display position
		'''
		global LEFT, BOTH

		if buttons is BOTH: # both -> move
			self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_SIZE))
			self.__campos[0] += dx
			self.__campos[1] += dy
		elif buttons is LEFT:

			# check if neuron is clicked
			neuron_i = self.__position_check([x,y],[n[1] for n in self.__neurons],epsilon=pow(self.NEURONSIZE*self.__camscale,2))
			if neuron_i >= 0:
				self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_HAND))
				self.__neurons[neuron_i][1][0] = (x-self.__campos[0])/self.__camscale
				self.__neurons[neuron_i][1][1] = (y-self.__campos[1])/self.__camscale
				for i in range(0,len(self.__connections)):
					if (self.__connections[i][0][0] == self.__connections[i][0][1]) and (self.__connections[i][0][0] == neuron_i):
						self.__connections[i][2] = self.__get_control_points(self.__neurons[neuron_i],self.__neurons[neuron_i],
							side = 1 if (self.__connections[i][2][0][0] > self.__connections[i][2][1][0]) else -1)
			else:
				# check if connection is selected
				connection_i = self.__position_check([x,y],[c[3] for c in self.__connections],epsilon=pow(self.ARROW_SIZE*self.__camscale,2))
				if connection_i >= 0:
					self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_HAND))
					if len(self.__connections[connection_i][2]) == 1:
						self.__connections[connection_i][2][0][0] += 2*dx/self.__camscale
						self.__connections[connection_i][2][0][1] += 2*dy/self.__camscale

	def on_mouse_motion(self,x,y,dx,dy):
		if self.__state[0] == 'neuron':
			self.__neurons[self.__state[1]][1][0] = (x-self.__campos[0])/self.__camscale
			self.__neurons[self.__state[1]][1][1] = (y-self.__campos[1])/self.__camscale
			for i in range(0,len(self.__connections)):
				if (self.__connections[i][0][0] == self.__connections[i][0][1]) and (self.__connections[i][0][0] == self.__state[1]):
					self.__connections[i][2] = self.__get_control_points(self.__neurons[self.__state[1]],self.__neurons[self.__state[1]],
						side = 1 if (self.__connections[i][2][0][0] > self.__connections[i][2][1][0]) else -1)
		elif self.__state[0] == 'connection':
			self.__connections[self.__state[1]][2][0][0] += 2*dx/self.__camscale
			self.__connections[self.__state[1]][2][0][1] += 2*dy/self.__camscale
		else:
			self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_DEFAULT))


	def on_mouse_press(self,x, y, button, modifiers):
		'''
		Recurrent connection is pressed: change the side
		Button is pressed: save, restore, or etc.
		'''
		button_i = self.__position_check([x,y],[[	(c[1][0]+self.BUTTONSIZE//2-self.__campos[0])/self.__camscale,
												(c[1][1]+self.BUTTONSIZE//2-self.__campos[1])/self.__camscale] for c in self.__buttons], 
												epsilon=pow(self.BUTTONSIZE//2,2))

		# button is selected
		if (button_i >= 0) and (button_i < len(self.__buttons)):
			if self.__button_names[button_i] is 'save': # first button = save
				self.__save_configuration()
				print('Save!')
			elif self.__button_names[button_i] is 'recover': # second button = recover
				self.__recover_configuration()

			elif self.__button_names[button_i] is 'background': # third button = change background
				self.__bgimage = self.__path+'images/bg.png' if self.__bgimage is None else None
				self.__change_button_state(button_i,state=0 if self.__bgimage is None else 1)
			
			elif self.__button_names[button_i] is 'record': # fourth button = start/stop recording
				self.__change_button_state(button_i,state=1 if not self.__record_state else 0)
				if not self.__record_state:
					if os.path.exists('frames'):
						shutil.rmtree('frames')
					self.out = cv2.VideoWriter(self.__path+'video.mp4', cv2.VideoWriter_fourcc(*'MP4V'), self.VIDEOFPS, (self.__window.width,self.__window.height))
				else:
					# combined multiple frames to video
					im_paths = glob.glob(self.__path+"frames/*.png")
					mx = max([int(im_paths[i][7:-4]) for i in range(0,len(im_paths))])
					del im_paths
					for i in range(2,mx):
						frame = cv2.imread(self.__path+"frames/"+str(i)+'.png')
						frame = frame[:,:self.__window.width-self.BUTTONSIZE,:]
						frame = cv2.resize(frame,(self.__window.width,self.__window.height))
						self.out.write(frame)
					self.out.release()
					shutil.rmtree('frames')
					print("Save Video!")
				self.__numframe = 0
				self.__record_state = not self.__record_state

			elif self.__button_names[button_i] is 'theme': # fifth button = change theme
				self.BGCOLOR = abs(self.BGCOLOR - 1.0)
				self.__change_button_state(button_i,state=1 if self.BGCOLOR > 0.5 else 0)
			
		else:
			# connection is clicked
			if self.__state[0] is 'free':
				neuron_i = self.__position_check([x,y],[n[1] for n in self.__neurons],epsilon=pow(self.NEURONSIZE*self.__camscale,2))
				if neuron_i >= 0:
					self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_HAND))
					self.__state[0] = 'neuron'
					self.__state[1] = neuron_i
				else:
					# check if connection is selected
					connection_i = self.__position_check([x,y],[c[3] for c in self.__connections],epsilon=pow(self.ARROW_SIZE*self.__camscale,2))
					if connection_i >= 0:
						if len(self.__connections[connection_i][2]) > 1:
							self.__connections[connection_i][2] = self.__get_control_points(self.__neurons[self.__connections[connection_i][0][0]],
							self.__neurons[self.__connections[connection_i][0][0]], 
							side = -1 if (self.__connections[connection_i][2][0][0] > self.__connections[connection_i][2][1][0]) else 1)
						else:
							self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_HAND))
							self.__state[0] = 'connection'
							self.__state[1] = connection_i
			elif self.__state[0] in ['neuron','connection']:
				self.__state[0] = 'free'
				self.__state[1] = None
				self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_DEFAULT))
					


			'''connection_i = self.__position_check([x,y],[c[3] for c in self.__connections])
			if connection_i >= 0:
				if len(self.__connections[connection_i][2]) > 1:
					self.__connections[connection_i][2] = self.__get_control_points(self.__neurons[self.__connections[connection_i][0][0]],
						self.__neurons[self.__connections[connection_i][0][0]], 
						side = -1 if (self.__connections[connection_i][2][0][0] > self.__connections[connection_i][2][1][0]) else 1)
			'''


	def on_mouse_release(self,x, y, button, modifiers):
		'''
		set mouse cursor to default when release mouse
		'''
		if self.__state[0] == 'free':
			self.__window.set_mouse_cursor(self.__window.get_system_mouse_cursor(self.__window.CURSOR_DEFAULT))

	def update_frame(self,dt):
		'''
		update frame and count fps
		update self.__numframe
		print every 5 seconds if VERBOSE is true
		'''
		self.__numframe += 1
		self.__numdisplayedframe += 1
		if self.VERBOSE:
			self.__frame_counter += 1
			if time.perf_counter()-self.__time_init >= 5:
				self.__fps = self.__frame_counter/(time.perf_counter()-self.__time_init)
				print('fps:',self.__fps,'Hz')
				self.__time_init = time.perf_counter()
				self.__frame_counter = 0

		if (self.__numframe%self.RECORDFREQUENCY == 0) and self.__record_state:
			if os.path.exists('frames'):
				pyglet.image.get_buffer_manager().get_color_buffer().save('frames/'+str(self.__numframe//self.RECORDFREQUENCY)+'.png')
			else:
				os.mkdir('frames')

	def on_draw(self):
		'''
		Clear the previous frame and draw the neu ones every time step.
		'''
		self.__timecounter = time.perf_counter()
		glClear(GL_COLOR_BUFFER_BIT)
		self.__background()
		#print(self.__campos,self.__neurons[-1][1])
		pyglet.gl.glClearColor(self.BGCOLOR,self.BGCOLOR,self.BGCOLOR,1.0)
		# draw connections in the bottom-most layer
		for i in range(0,len(self.__connections)):
			#try:
			if 1:
				connect = self.__connections[i]
				nodes = self.__connection2nodes(connect)
				do = False
				for j in range(0,len(nodes[0])):
					x = nodes[0][j]*self.__camscale+self.__campos[0] 
					y = nodes[1][j]*self.__camscale+self.__campos[1]
					if ((x >= 0) and (x <= self.__window.width)) or ((y >= 0) and (y <= self.__window.height)):
						do = True
						break
				if do:
					activity = [np.sign(connect[1])*(self.__activity_bias[connect[0][0]]+self.__activity_gain[connect[0][0]]*self.__neurons[connect[0][0]][2]),
					np.clip(np.abs(connect[1]),0,1)*np.sign(connect[1])*(self.__activity_bias[connect[0][0]]+self.__activity_gain[connect[0][0]]*self.__neurons[connect[0][0]][2])]
					grad = abs(self.__connections[i][1] - self.__previous_weight[i])
					if grad >= 1e-5:
						self.__add_connection(nodes,np.clip(1.0+self.GRADIENTMULTIPLIER[0]*grad,1.0,1000.0)*connect[1],[-1.0,-1.0],self.ARROW_SIZE,white=np.clip(self.GRADIENTMULTIPLIER[1]*grad,0.1,0.5))
						self.__previous_weight[i] = self.__connections[i][1]

						
					arrow_pos = self.__add_connection(nodes,connect[1],activity,self.ARROW_SIZE)
					if arrow_pos is not None:
						self.__connections[i][3] = arrow_pos
			#except:
				#pass
		
		# draw neurons in the middle layer
		for i in range(0,len(self.__neurons)):
			
			try:
				neuron = self.__neurons[i]
				x = neuron[1][0]*self.__camscale+self.__campos[0] 
				y = neuron[1][1]*self.__camscale+self.__campos[1] 
				s = self.NEURONSIZE*self.__camscale
				if ((x+s >= 0) and (x-s <= self.__window.width)) or ((y+s >= 0) and (y-s <= self.__window.height)):
					self.__add_neuron(neuron[0],neuron[1],self.NEURONSIZE,neuron[2],outlineThickness=self.OUTLINETHICKNESS,gain=self.__activity_gain[i],bias=self.__activity_bias[i])
					
			except:
				pass
		# draw the button in the front-most layer
		for i in range(0,len(self.__buttons)):
			self.__buttons[i][1] = [self.__window.width-self.BUTTONSIZE,self.__window.height-(self.BUTTONSIZE*0.9)*(i+1)]
			self.__button(self.__buttons[i][2],self.__buttons[i][1][0],self.__buttons[i][1][1])


		

	''' ---------------- Private methods for drawing ---------------------- '''
	def __circle(self,x, y, radius,color=(1.0,1.0,1.0),outlineThickness=0):
		'''
		Draw a circle of size "radius" at position (x,y)
		The color of the circle is defined by tuple "color" (R,G,B) (float; 0.0-1.0)
		The outline is white for dark theme and black for light theme, if "outlineThickness" > 0
		'''
		s = sin(2*pi / self.NCIRCLEEDGES)
		c = cos(2*pi / self.NCIRCLEEDGES)
		dx, dy = radius, 0
		glBegin(GL_POLYGON) # draw polygon
		gl.glColor3f(color[0], color[1], color[2]);
		glVertex2f(x, y)
		for i in range(self.NCIRCLEEDGES+1):
			glVertex2f(x+dx, y+dy)
			dx, dy = (dx*c - dy*s), (dy*c + dx*s)
		glEnd()

	def __bezier_curve(self,nodes,arrow_size,color1=(0.0,1.0,0.0,0.0),color2=(1.0,0.0,0.0,0.0),width=2,n_points=20):
		'''
		Draw a curve connecting between multiple nodes given in a list named "nodes" and draw a 
		arrow of size "arrow_size".
		The color of the curve is defined by tuple "color" (R,G,B) (float; 0.0-1.0)
		The width of the curve is defined by "width" 
		The resolution of curve is defined by number of intermediate pointds "n_points"
		'''
		points = bezier.Curve(nodes, degree=nodes.shape[1]-1).evaluate_multi(np.linspace(0.0, 1.0, n_points))
		
		glLineWidth(width)
		glBegin(GL_LINE_STRIP) # draw line/curve
		#glEnable(GL_BLEND)
		#glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
		gl.glColor4f(color1[0], color1[1], color1[2],color1[3])
		glVertex2f(points[0][0],points[1][0])
		for i in range(1,n_points):
			gain = (n_points-i)/n_points
			gl.glColor4f(color1[0]*gain+color2[0]*(1-gain),color1[1]*gain+color2[1]*(1-gain),color1[2]*gain+color2[2]*(1-gain),color1[3])
			glVertex2f(points[0][i],points[1][i])
		
		glEnd()
		# draw arrow with oriantation "alpha" at (points[0][n_points//2],points[1][n_points//2])
		i = n_points//2
		gain = (n_points-i)/n_points
		gl.glColor4f(color1[0]*gain+color2[0]*(1-gain),color1[1]*gain+color2[1]*(1-gain),color1[2]*gain+color2[2]*(1-gain),color2[3])
		alpha = -atan2((points[0][i]-points[0][i-1]),(points[1][i]-points[1][i-1])) + 1.5708
		arrow_position = [points[0][i-1]+arrow_size*cos(alpha)/2, points[1][i-1]+arrow_size*sin(alpha)/2]
		if color2[3] == 1.0:
			self.__arrow(points[0][i-1],points[1][i-1],alpha,size=arrow_size)		
			return arrow_position
		return None

	def __arrow(self,x,y,alpha,size):
		'''
		Draw a arrow with orientation "alpha" and size "size" at position (x,y)
		'''
		glBegin(GL_POLYGON) # draw triangle
		glVertex2f(x-size*sin(alpha)/2, y+size*cos(alpha)/2)
		glVertex2f(x+size*cos(alpha), y+size*sin(alpha))
		glVertex2f(x+size*sin(alpha)/2, y-size*cos(alpha)/2)
		glEnd()

	def __button(self,image,x,y):
		'''
		Draw a button using "image" at (x,y)
		'''
		pyglet.sprite.Sprite(image,x=x,y=y).draw()

	def __change_button_state(self,index,state=0):
		'''
		Change the button image at index "index" to "state" and redraw it
		state = 0 means unpressed
		state = 1 mean pressed
		'''
		if 1:
			self.__buttons[index][2] = pyglet.resource.image('images/bt'+str(index)+'.png') if state == 0 else pyglet.resource.image('images/bt'+str(index)+'_pressed.png')
			self.__buttons[index][2].width= self.BUTTONSIZE
			self.__buttons[index][2].height = self.BUTTONSIZE

	def __background(self):
		'''
		Draw an image from a path "__bgimage" with background opacity "BGOPACITY" if it exists
		"__bgimage" is fixed to 'images/bg.png'. Therefore, if you want to choose your image, please
		rename it to 'bg.png' and put it at 'images/bg.png'.
		'''
		if self.__bgimage is not None:
			if (self.__previous_windowsize[0] != self.__window.width) or (self.__previous_windowsize[1] != self.__window.height) or (self.__bg is None):
				self.__bg = pyglet.resource.image(self.__bgimage)
				self.__previous_windowsize[0] = self.__window.width
				self.__previous_windowsize[1] = self.__window.height 
				self.__bg.width= self.__window.width
				self.__bg.height = self.__window.height
			spr = pyglet.sprite.Sprite(self.__bg,x=0,y=0)
			spr.opacity=int(255*self.BGOPACITY)
			spr.draw()
		else:
			self.__bg = None

	def __add_neuron(self,name,position,size,activity,outlineThickness=0.0,gain=1,bias=0):
		'''
		Draw a neuron with
		name = name of the neuron (suggest: N1, N2, C1, B (bias))
		position = [x,y] where x and y are the positions in x (horizontal) axis and y (vertical) axis
		size = size of the neuron
		activity = activity of the neuron
		outlineThickness = outline thickness of the neuron
		gain and bias = activity gain and bias
		'''
		if 1:#abs(gain*activity+bias) > 0.01:
			x = position[0]*self.__camscale+self.__campos[0]
			y = position[1]*self.__camscale+self.__campos[1]
			size = size*self.__camscale
			''' ------- draw outline ----- '''
			_ = self.__circle(x,y,size+outlineThickness*self.__camscale,color=(1.0-self.BGCOLOR,1.0-self.BGCOLOR,1.0-self.BGCOLOR),outlineThickness=0) if outlineThickness > 0 else None
			''' ------- draw neuron ----- '''

			_ = self.__circle(x,y,size,color=self.__activity2color(gain*activity+bias))
			''' ------- neuron name ------ '''
			if (self.NEURONNAME) :
				if (self.__text is None):
					self.__text = pyglet.text.Label(name,font_name='CrimsonPro',font_size=size//1.5,x=x, y = y,anchor_x='center', anchor_y='center',color=(0,0,0,255) ).draw() 
				self.__text.draw()
	def __add_connection(self,nodes,size,activity,arrow_size,white=None):
		'''
		Draw a connection between two neurons with
		nodes = list of the position of two neurons
		size = width/size of the connection
		activity = variable determining color of the connection (depend on sender activation)
		arrow_size = size of arrow
		'''
		if self.OMIT:
			epsilon = 0.01
		else:
			epsilon = -1
		if (abs(activity[0]) > epsilon):
			nodes = np.array(nodes)
			nodes[0] = nodes[0]*self.__camscale + self.__campos[0]
			nodes[1] = nodes[1]*self.__camscale + self.__campos[1]
			nodes = nodes.astype(np.int)
			if abs(size) < 1e-8:
				return (0,0)
			
			
			if white == None:
				size = min(self.CONNECTIONWIDTH[1],abs(self.CONNECTIONWIDTH[2]*size)+self.CONNECTIONWIDTH[0])*self.__camscale
				c1 = self.__activity2color(activity[0],gain=min((self.__camscale-0.05)/0.5,self.MAXCONNECTIONALPHA))
				c2 = self.__activity2color(activity[1],gain=min((self.__camscale-0.05)/0.5,self.MAXCONNECTIONALPHA))
			else:
				size = abs(self.CONNECTIONWIDTH[2]*size)+self.CONNECTIONWIDTH[0]*self.__camscale
				if self.BGCOLOR <= 0.5:
					c1 = c2 = (white,white,white)
				else:
					c1 = c2 = (1.0,1.0,1.0-white*2)
			arrow_position = self.__bezier_curve(nodes,arrow_size*self.__camscale,width=size,
				color1=(c1[0],c1[1],c1[2],1.0),
				color2 = (c2[0],c2[1],c2[2],1.0),n_points=int(self.NBEZIERPOINTS))

			return ((arrow_position[0]-self.__campos[0])/self.__camscale,(arrow_position[1]-self.__campos[1])/self.__camscale)
		else:
			return None

	''' ----------- Private methods for preprocessing data ---------------- '''
	def __Cmatrix2adjacency(self,C):
		'''
		Convert a C-matrix (connection matrix; with synaptic weight) to adjacency matrix (only connecti)
		'''
		C = np.abs(np.array(C))
		C[C > 0.0] = 1.0
		C[C == 0.0] = 100.0
		for i in range(0,C.shape[0]):
			for j in range(i,C.shape[1]):
				C[i][j] = min(C[i][j],C[j][i])
				C[j][i] = min(C[i][j],C[j][i])
		return C

	def __Cmatrix2layout(self,C,pos=None):
		'''
		Convert a C-matrix (connection matrix; with synaptic weight) to a layout (position of each neuron/node)
		"pos" determines fixed position of each neuron. If it is None, then the initial position is randomed and
		updated.
		Type of layout is defined by self.LAYOUT, which includes 'kawai' (recommended), 'spring', 'spectral',
		and 'circular'
		The aim is to get the best (or good enough) initial layout and we can manually modify later on.
		'''
		A = self.__Cmatrix2adjacency(C)
		G = nx.from_numpy_matrix(A, create_using=nx.Graph)#nx.DiGraph)
		if self.LAYOUT is 'kawai':
			layout = nx.kamada_kawai_layout(G,pos=pos)
		elif self.LAYOUT is 'spring':
			layout = nx.spring_layout(G,pos=pos)
		elif self.LAYOUT is 'spectral':
			layout = nx.spectral_layout(G,pos=pos)
		elif self.LAYOUT is 'circular':
			layout = nx.circular_layout(G,pos=pos)
		else:
			sys.exit("layout error!")
		return layout

	def __connection2nodes(self,connection):
		'''
		Convert list of connection (from self.__connections) to tuple of nodes (x,y)
		'''
		x = [0]*(2+len(connection[2]))
		y = [0]*(2+len(connection[2]))
		x[0], y[0] = self.__neurons[connection[0][0]][1]
		for i in range(1,len(connection[2])+1):
			x[i], y[i] = connection[2][i-1]
		x[-1], y[-1] = self.__neurons[connection[0][1]][1]
		return (tuple(x),tuple(y))

	def __activity2color(self,activity,gain=1):
		'''
		Convert neurons/connection activity to color representing that activity
		Gain denote the opacity gain (0.0 - 1.0)
		'''
		lowerbound = abs(self.INACTIVATECOLOR-self.BGCOLOR)

		#activity = activity*activity*activity/abs(activity) if activity != 0 else 0
		if activity < 0.0:
			redness = -1*min(activity,0.0)
			r = redness+lowerbound 
			gb = lowerbound-self.BGCOLOR*redness
			return (r*gain+(1-gain)*self.BGCOLOR,gb*gain+(1-gain)*self.BGCOLOR,gb*gain+(1-gain)*self.BGCOLOR)
		else:
			greeness = max(activity,0.0)
			g = greeness+lowerbound
			rb = lowerbound-self.BGCOLOR*greeness
			return (rb*gain+(1-gain)*self.BGCOLOR,g*gain+(1-gain)*self.BGCOLOR,rb*gain+(1-gain)*self.BGCOLOR)

	def __get_control_points(self,neuronA,neuronB,side=1):
		'''
		Get control points connecting between two neurons (from neuronA to neuronB)
		If the connection is recurrent then side determines whether the recurrent connection is
		to the left or to the right of the neuron.
		'''
		if neuronA == neuronB:
			r = self.RECURRENTRADIUS
			return [[neuronA[1][0]-side*r/2.5,neuronA[1][1]-r],[neuronA[1][0]-side*r,neuronA[1][1]],[neuronA[1][0]-side*r/2.5,neuronA[1][1]+r]]
		op = [[neuronA[1][0],neuronB[1][1]]]
		return op

	def __position_check(self,click,lis,epsilon=1000):
		'''
		Check which neuron/connection provided in "lis" is selected by using a threshold of "epsilon"
		Return the index of the selected elements.
		click = [x,y]
		lis = [[x,y], ...]
		'''
		for i in range(0,len(lis)):
			similarity = pow(lis[i][0]*self.__camscale + self.__campos[0]-click[0],2)+pow(lis[i][1]*self.__camscale + self.__campos[1]-click[1],2)
			if similarity < epsilon:
				return i
		return -1 

	''' ---- Private methods for saving and loading configuration -------- '''
	def __save_configuration(self):
		'''
		Save configuration (neuron position, connection, control points)
		'''
		f = open(self.__path+self.__confpath, "w")

		# neuron position
		for i in range(0,len(self.__neurons)):
			f.write(str(self.__neurons[i][0])+','+str(self.__neurons[i][1][0])+','+str(self.__neurons[i][1][1])+'/')
		f.write('#')

		# connection
		points = []
		for i in range(0,len(self.__C_matrix)):
			for j in range(0,len(self.__C_matrix[i])):
				f.write(str(self.__C_matrix[i][j])+',')
			points.append([[[0,0]]]*len(self.__C_matrix[0]))
			f.write('/')
		f.write('#')

		# control points
		for k in range(0,len(self.__connections)):
			points[self.__connections[k][0][0]][self.__connections[k][0][1]] = self.__connections[k][2]
		for i in range(0,len(points)):
			for j in range(0,len(points[i])):
				for k in range(0,len(points[i][j])):
					f.write(str(points[i][j][k][0])+','+str(points[i][j][k][1])+"&")
				f.write(';')
			f.write('/')
		f.close()

	def __recover_configuration(self):
		'''
		Recover configuration (neuron positon, connection, and control points)
		'''
		print(self.__path+self.__confpath)
		f = open(self.__path+self.__confpath,'r')
		data = f.read().split('#')

		# neuron position
		neurons = data[0].split('/')[:-1]

		for i in range(0,len(neurons)):
			n = neurons[i].split(',')
			self.__neurons[i][0] = n[0]
			self.__neurons[i][1] = [float(n[1]),float(n[2])]

		# connections

		connections = data[1].split('/')[:-1]
		for i in range(0,len(connections)):
			connections[i] = connections[i].split(',')
			for j in range(0,len(connections[i])-1):
				self.__C_matrix[i][j] = float(connections[i][j])

		self.set_connection(self.__C_matrix,change_layout=False,reset_connection_parameter=True)

		# control points
		points = data[2].split('/')[:-1]
		for i in range(0,len(points)):
			points[i] = points[i].split(';')[:-1]
			for j in range(0,len(points[i])):
				points[i][j] = points[i][j].split('&')[:-1]
				if points[i][j] != '0,0':
					for k in range(len(points[i][j])):
						points[i][j][k] = points[i][j][k].split(',')
						points[i][j][k] = [float(points[i][j][k][0]),float(points[i][j][k][1])]
				else:
					points[i][j] = None

				if points[i][j] != None:
					for k in range(0,len(self.__connections)):
						if (self.__connections[k][0][0] == i) and (self.__connections[k][0][1] == j):
							self.__connections[k][2] = points[i][j]
							break
		f.close()

	











